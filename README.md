# @commonshost/server 🕸

Static HTTP/2 webserver for single page apps and progressive web apps.

- HTTP/2, HTTP/1.1, and HTTP/1.0
- Content encoding for Brotli and Gzip compression
- Server Push Manifests & Cache Digest
- Scalable, multi-processor clustering
- Auto-generate HTTPS certificate for localhost development
- Fallback for client side routing
- HTTP to HTTPS redirect
- CORS
- Immutable caching of revved files

## Installation

```
npm install @commonshost/server
```

## CLI

```
./node_modules/.bin/server <command> [options]
```

Tip: In NPM `scripts` the relative path (`./node_modules/.bin/`) may be omitted. Looks neater.

#### `--version`, `-v`

Display the current version number.

#### `--help [command]`, `-h [command]`

List global or command-specific options.

### Command: `start`

Start the server as a long running process that spawns workers.

Aliases: `run`, `up`, `launch`

#### `$PORT`

The environment variable `PORT` sets the network port for incoming HTTPS connections.

The `https.port` and `http.to` options in the configuration file are set to `$PORT`.

The `http.from` port is derived by setting the last three decimal digits of `$PORT` set to `080` or `000`. E.g. HTTPS `8443` -> HTTP `8080`, HTTPS `443` -> HTTP `80`, HTTPS `10080` -> HTTP `10000`.

#### `--options [file]`

Configuration file path. Defaults to: `./commons((.)host)(.conf(ig)).js(on)`

If no configuration file is found, the default configuration is used. See the [Configuration](#configuration) section for details.

The default configuration loads `./serverpush.json` as a server push manifest, if it exists. See [`@commonshost/manifest generate`](https://www.npmjs.com/package/@commonshost/manifest#generate-a-manifest) for details.

#### `--watch`/`--no-watch`

Defaults to `false`, which is the same as specifying the `--no-watch` option.

The server maintains an in-memory index of all files. Compared to dynamic filesystem scanning, this dramatically improves performance when pushing many small files. However if files and directories are added, moved, or deleted, the index becomes incorrect. Pass the `--watch` option to monitor filesystem changes and automatically rebuild the index. This has some performance cost, depending on operating system specific support, so watching is disabled by default. Watching is typically only used in development mode, not production.

### Command: `stop`

Shuts down the server by sending it the `SIGINT` signal.

Aliases: `halt`, `exit`, `quit`, `kill`, `down`, `end`

### Command: `reload`

Gracefully restart workers with a new configuration by sending the server the `SIGHUP` signal.

Aliases: `restart`, `refresh`, `renew`, `update`, `cycle`, `load`

### Command: `test`

Loads and validates a server configuration.

Aliases: `verify`, `validate`, `check`, `configtest`, `lint`

The process prints `PASS` and exits `0` if the configuration is valid, or otherwise prints `FAIL` with an error message and exits `1`.

#### `--options [file]`

Identical to the same option of the `start` command.

## Signals

If the server is started from the command line, it can be controlled by sending process signals.

### SIGINT

Graceful shutdown of all workers and the master process.

### SIGHUP

Gracefully restart all workers with reloaded configurations.

## JavaScript API

### Constructor

```js
const {Master} = require('@commonshost/server')
const server = new Master(configuration)
```

The `configuration` argument contains:

- `options`: The [server configuration](#configuration) object.
- `generateCertificate`: Defaults to `false`. If `true`, a self-signed, trusted certificate for `DNS:localhost` is created on startup in case `https.key` and `https.cert` are missing.
- `serveDefaultSite`: Defaults to `true`. Creates a fallback host configuration using default settings if no `hosts` are explicitly configured. This is a convenience feature for CLI usage so that no configuration is needed by default.
- `cwd`: Defaults to the current working directory (`./`). File and directory paths in the configuration are relative to this path.
- `configurationFilepath`: Defaults to `undefined`. Set this to a file path to enable external server push manifests. External manifests are resolved relative to this file path.

### Starting

```js
await server.listen()
```

Loads the configuration and spawns worker processes to listen for incoming connections. Also starts a redirecting server from HTTP to HTTPS.

### Stopping

```js
await server.close()
```

Shuts down the workers and redirecting server gracefully.

### Reloading

```js
await server.reload()
```

Starts new workers to gracefully replace the old ones without downtime. Reloads the configuration.

### Run-Time Updates

```js
await server.message(message)
```

Invalidates cached information about files, certificates, and configuration on a per-domain level. Used to update at run-time without the performance cost of a full reload. See [Messaging](#messaging) for details.

## Messaging API

Messages are used to notify the workers of changes without reloading them. This is more efficient than spawning workers.

Messages can be sent programmatically, using the `.message(...)` method. Messages are also sent by the CLI (i.e. `reload`, `watch`, `stop` commands) and the edge agent which receives them from the core api service.

All messages require at least the `type`, `domain`, and `root` properties.

- `type` identifies what kind of information is described. The supported types are defined below.
- `domain` is the host name of the site to which this message applies.
- `root` is the path of base directory containing the site's files.

Additionally, the `configuration` field contains a valid [configuration](#configuration) document. Only the host options matching the `domain` are applied, others are ignored. The `manifest` property can not reference an external file; only an array of inline server push rules is allowed.

For example:

```js
{
  type: 'site-deploy',
  domain: 'example.net',
  root: '/var/www/html',
  configuration: [{
    domain: 'example.net',
    fallback: {200: '/index.html'}
  }]
}
```

### Type: `site-deploy`

- `isNewDomain` Set to `true` if this domain name was not previously served. Otherwise `false`. Not currently used.
- `hasNewFiles` Set to `true` if any files are changed, added, or removed. Otherwise `false`.
- `hasNewConfiguration` Set to true if any option in the site configuration is modified.

```js
{
  type: 'site-deploy',
  domain: 'example.net',
  isNewDomain, // boolean
  hasNewFiles, // boolean
  hasNewConfiguration // boolean
}
```

### Type: `site-delete`

A site has been removed. Clear any cached information for the domain.

```js
{
  type: 'site-delete',
  domain: 'example.net'
}
```

### Type: `configuration-update`

New configuration options are used for the domain.

```js
{
  type: 'configuration-update',
  domain: 'example.net'
}
```

### Type: `file-delete`

One or more files have been removed. Clear their cached information.

```js
{
  type: 'file-delete',
  domain: 'example.net',
  files: [
    {path: 'foo/bar.lol'},
    {path: 'abc/def/ghi.jkl'},
    {path: '123.xyz'}
  ]
}
```

### Type: `file-update`

One or more files have been changed. Reset or reload their cached information.

```js
{
  type: 'file-update',
  domain: 'example.net',
  files: [
    {path: 'foo/bar.lol'},
    {path: 'abc/def/ghi.jkl'},
    {path: '123.xyz'}
  ]
}
```

### Type: `certificate-issue`

A new certificate for the domain is available. Clear any previously cached certificate.

```js
{
  type: 'certificate-issue',
  domain: 'example.net'
}
```

### Type: `certificate-revoke`

The previous certificate for the domain is no longer in use. Clear any cached information.

```js
{
  type: 'certificate-revoke',
  domain: 'example.net'
}
```

## Configuration

See [`@commonshost/configuration`](https://www.npmjs.com/package/@commonshost/configuration) for the JSON Schema, validation, and normalisation tools.

There are three layers in the configuration:
- **Server Options**: Applies globally to all sites on the server.
- **Host Options**: Applies to a single domain.
- **HTTP/2 Server Push Manifest**: Also applies to a single domain, but defined as a separate specification.

### Server Options

#### `signature`

Default: `true`

Boolean value that adds the `Server:` header to all HTTP responses if `true`, and omits that header if `false`.

The value of the header identifies the versions of the server, Node.js, nghttp2, and the operating system.

Often disabled for reasons of paranoia or placebo performance tuning.

#### `acme`

Settings related to the [Automated Certificate Management Environment (ACME)](https://datatracker.ietf.org/wg/acme/) protocol, i.e. support for the [LetsEncrypt](https://letsencrypt.org) certificate authority. This affects all requests, across all hosted domains, where the pathname starts with [`/.well-known/acme-challenge/`](https://www.iana.org/assignments/well-known-uris/well-known-uris.xhtml). There are many ways to implement ACME support. These settings offer flexibility in issuing certificates and serving the correct certificate for each host.

When a certificate is requested for a domain using the ACME HTTP challenge, the certificate authority (i.e. LetsEncrypt's [Boulder](https://github.com/letsencrypt/boulder) server) resolves the given DNS hostname and makes an HTTP request. The webserver needs to respond with values only known by the entity that requested the certificate. In a simple case of one webserver this can be handled by serving a local directory through `acme.webroot`. But when DNS records point to multiple edge servers, it is necessary to relay or deflect the challenge request, via `acme.proxy` or `acme.redirect` respectively, to the certificate requesting entity.

Any domains that do not have a certificate, as per `acme.key` and `acme.cert` lookups, are served using the fallback certificate in `https.key` and `https.cert`.

#### `acme.proxy`

Default: `''`

If a valid URI is specified, the server proxies all ACME challenge requests to this upstream server.

Proxying requires the server to do more work than a redirect, but it is transparent to the CA. This allows use of ports other than `80` and `443`, to which Boulder is restricted.

This string is prepended to the challenge request's URL path, so omit any trailing shash to avoid duplication. Both HTTP and HTTPS can be used, depending on the scheme (`http:` vs `https:`).

Example:

```js
{
  acme: {
    proxy: 'https://vault.example.net:8443'
  }
}
```

#### `acme.redirect`

Default: `''`

If a valid URI is specified, the server redirects, through a [`301 Permanent Redirect`](https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml) response, all ACME challenge requests to this upstream server.

Redirecting is light on the edge server, but requires a second connection from Boulder to the specified server, possibly adding some latency.

Both HTTP and HTTPS can be used, depending on the scheme, i.e. `http:` or `https:` respectively. Note that Boulder, the LetsEncrypt reference server, is currently restricted to ports `80` and `443`. Redirecting to any other ports is not supported, and should be handled through `acme.proxy`.

This string is prepended to the challenge request's URL path. Omit any trailing shash to avoid duplication.

Example:

```js
{
  acme: {
    redirect: 'https://vault.example.net'
  }
}
```

#### `acme.webroot`

Default: `''`

If a valid path is specified, the server responds to challenges by mapping to static files in this directory. This is intended to work with the `webroot` option of many ACME clients, where files are generated during the challenge-response process.

```js
{
  acme: {
    webroot: '/var/www/html'
  }
}
```

#### `acme.store`

Default: `''`

Directory where keys and certificates are stored.

Example:

```js
{
  acme: {
    webroot: '/etc/ssl'
  }
}
```

#### `acme.key`

Default: `'$store/$domain/key.pem'`

Path to the domain-specific key file.

May contain substitution variables `$store` and `$domain`.

Example:

```js
{
  acme: {
    key: '$store/sites/$domain/crypto/key.pem'
  }
}
```

#### `acme.cert`

Default: `'$store/$domain/cert.pem'`

Path to the domain-specific certificate file.

May contain substitution variables `$store` and `$domain`.

Example:

```js
{
  acme: {
    cert: '$store/sites/$domain/crypto/cert.pem'
  }
}
```

#### `log`

Settings for the [Pino](https://getpino.io)-based logger.

#### `log.level`

Default: `'info'`

Minimum threshold for log messages to be recorded. One of `fatal`, `error`, `warn`, `info`, `debug`, `trace`; or `silent` to disable logging.

#### `http`

An object containing settings for the automatic redirection of HTTP to HTTPS. This is necessary since HTTP/2 is only supported by browsers when using a TLS connection (TLS).

#### `http.redirect`

Default: `true`

If `true`, an HTTP server listens to redirect requests to HTTPS.

If `false`, no HTTP server is started. This also means ACME is disabled, since it is served over HTTP as well.

#### `http.from`

Default: `8080`

The port number where the HTTP server accepts connections.

#### `http.to`

Default: `8443`

The port number of the HTTPS URLs to which HTTP traffic is redirected.

Redirects use the `308 Permanent Redirect` status code.

#### `https`

Settings for the fallback TLS context that is used if no files match `acme.key` and `acme.cert`.

Useful when serving subdomains using a wildcard certificate.

If no fallback exists on launch, a self-signed certificate and key pair is generated using [tls-keygen](https://www.npmjs.com/package/tls-keygen). The certificate is attempted to be added to to the operating system trusted store, which may require user confirmation and entry of their password. This is only done in CLI mode, or if the `generateCertificate` API argument is `true`; otherwise the server simply fails to launch.

See the corresponding options of [tls.createSecureContext](https://nodejs.org/api/tls.html#tls_tls_createsecurecontext_options).

#### `https.key`

Default: `'~/.commonshost/key.pem'`

A string of the path to a PEM file containing the secret key.

#### `https.cert`

Default: `'~/.commonshost/cert.pem'`

A string of the path to a PEM file containing the public certificate.

#### `https.ca`

Default: `[]`

An array of strings containing the path to all files in the certificate chain.

#### `https.port`

Default: `8443`

The TCP/IP port number for incoming TLS connections.

The [standard HTTPS port](https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?search=https) is `443` for both HTTP/1.1 and HTTP/2. However, on Unix (Linux/MacOS), root permissions are required to listen on port numbers from 1 to 1024. The default value is outside this restricted range.

#### `placeholder`

By default, requests for sites that are not configured under hosts are responded to with a `404` status and `Not Found` error message.

#### `placeholder.path`

File path to an HTML response to be served for missing domains.

Only a single file is allowed. Any required assets should either be inlined or hosted on an external domain.

The placeholder is only served if the request accepts an HTML response and the pathname either has no file extension or ends in `.html`.

#### `workers`

The server runs one master process that controls one or more worker processes. The workers process incoming requests and generate responses. Communication between the master and workers uses Node.js [cluster](https://nodejs.org/api/cluster.html) module message passing.

#### `workers.count`

Default: `'max_physical_cpu_cores'`

The desired number of worker processes to spawn at launch. Must be a number or a [mathematical expression](https://www.npmjs.com/package/expr-eval) that evaluates to a number.

Examples:

Single worker process:

```js
workers: {
  count: 1
}
```

Invalid: must spawn at least 1 worker.

```js
workers: {
  count: 0
}
```

One worker per CPU core:

```js
workers: {
  count: 'max_physical_cpu_cores'
}
```

Half as many workers as CPU cores:

```js
workers: {
  count: 'max_physical_cpu_cores / 2'
}
```

Leave one core for other processes:

```js
workers: {
  count: 'max_physical_cpu_cores - 1'
}
```

#### `hosts`

Default: `[]`

An array of objects containing [host options](#host-options) configurations.

### Host Options

#### `hosts[].domain`

Default: `''` or `'localhost'` (only for the default site, see [serveDefaultSite](#constructor))

The DNS hostname of the site. May be specified as an Internationalized Domain Name (IDN) containing non-ASCII characters.

Examples:

```js
{
  hosts: [
    { domain: 'example.net' },
    { domain: 'xn--yfro4i67o.example.net' },
    { domain: '🦄.example.net' }
  ]
}
```

#### `hosts[].root`

The path to the base directory containing static files to serve.

If no root is specified, the server tries to auto-detect static site generator or packaging tool output directories. For example: `./dist`, `./public`, `./_site`, and many more.

If no directory is auto-detected, the current working directory is used. A warning message is logged to indicate this fallback behaviour.

#### `hosts[].fallback`

Default: `{}` (no fallback)

An object mapping HTTP status code to file paths.

Fallbacks are supported for:

- `200` to serve a missing file as a success response. Typically used for client side routing.
- `404` to serve a missing file as an error response. Typically used for client side routing.

Example:

```js
{
  fallback: {
    200: './index.html'
  }
}
```

#### `hosts[].cacheControl`

Sets the HTTP caching headers.

#### `hosts[].cacheControl.immutable`

Default: `[]`

An array of path [globs](https://www.npmjs.com/package/micromatch) for immutable files.

Special named patterns can be used as shorthand. These are:

- `hex` — Matches hexadecimal hash revved files. Example: `layout-d41d8cd98f.css`
- `emoji` — Matches emoji revved files. Example: `app.⚽️.js`

By default, all non-immutable responses have the header:

```
cache-control: public, max-age=1, must-revalidate
```

File paths that match the patterns set by the `cacheControl.immutable` option are considered to *never, ever* change their contents. To tell browsers never to revalidate these resources, they are served with the header:

```
cache-control: public, max-age=31536000, immutable
```

Examples:

```js
{
  cacheControl: {
    immutable: [
      '/library/v1.2.3/**/*.js'
    ]
  }
}
```

#### `hosts[].directories`

Behaviour of directory paths.

#### `hosts[].directories.trailingSlash`

Default: `'always'`

Enforces a consistent *clean URL* for directory paths.

The value is a string that can be:
- `'always'` to use HTTP redirects to append a `/` at the end of the directory name.
- `'never'` to use HTTP redirects to strip any `/` at the end of the directory name.

#### `hosts[].accessControl`

Settings related to CORS.

#### `hosts[].accessControl.allowOrigin`

Default: `'*'`

If specified, sets this value as the `access-control-allow-origin` header on every response.

#### `hosts[].serviceWorker`

Settings related to service workers.

#### `hosts[].serviceWorker.allowed`

Default: `'/'`

If specified, sets this value as the `service-worker-allowed` header on every response.

#### `hosts[].strictTransportSecurity`

Settings related to HTTP Strict Transport Security.

#### `hosts[].strictTransportSecurity.maxAge`

Default: `5184000` (60 days)

Sets this value as the `strict-transport-security` header on every response.

#### `hosts[].manifest`

The `manifest` property declares which files or URLs need to be pushed as dependencies for any request. Using HTTP/2 Server Push (`PUSH_PROMISE` frames) can eliminate round trips between browser and server and speed up resource loading.

The `manifest` property type can be:

- **Array**: *Inline manifest*, see the [HTTP/2 Server Push Manifest](https://www.npmjs.com/package/@commonshost/manifest) specification for details and examples.
- **String**: *File path* of an external manifest. This path is relative to the server configuration file.

Example: Inline manifest

```js
{
  hosts: [
    domain: 'localhost',
    root: './dist',
    manifest: [
      // Example:
      {
        // When serving the homepage,
        get: '**/*.html',
        // push all CSS and JS files.
        push: ['**/*.css', '**/*.js']
      }
    ]
  ]
}
```

Example: External manifest file

Using an external file is useful when using a build tool that traces dependencies to automatically generate the manifest.

This example shows a static website in the `./dist` directory. The first section runs the [`@commonshost/manifest`](https://www.npmjs.com/package/@commonshost/manifest#cli) command line interface (CLI) tool to trace all HTML/JS/CSS files for dependencies. The automatically generated manifest is stored as `./manifest`. The second section shows a reference to this external manifest.

```
$ npx @commonshost/manifest generate ./dist ./manifest.json
```

```js
{
  hosts: [{
    manifest: './manifest.json'
  }]
}
```

## Tutorials

### Server Push with Cache Digests

Page load time is a largely function of latency (round trip time × delays) and aggregate volume (number × size of assets).

Latency is minimised by using HTTP/2 Server Push to deliver any necessary assets to the browser alongside the HTML. When the browser parses the HTML it does not need to make a round trip request to fetch styles, scripts, and other assets. They are already in its cache or actively being pushed by the server as quickly as network conditions allow.

Volume is reduced by using strong compression (HPACK, Brotli, etc), and by avoiding sending redundant data.

If all assets were pushed every time, a large amount of bandwidth would be wasted. HTTP/1 asset concatenation makes a tradeoff between reducing round trips (good) and re-transferring invalidated, large files (bad). For example having to re-tranfer an entire spritesheet or JavaScript bundle because of one little change.

The HTTP/1 approach was to use file signatures (Etags) and timestamps to invalidate cached responses. This requires many expensive round trips where the browser checks with the server if any files have been modified.

Cache Digests to the rescue! Using a clever technique, called Golomb-Rice Coded Bloom Filters, a compressed list of cached responses is sent by the browser to the server. Now the server can avoid pushing assets that are fresh in the browser's cache.

With Server Push and Cache Digests the best practice is to have many small files that can be cached and updated atomically, instead of large, concatenated blobs.

Browsers do not yet support cache digests natively so Service Workers and the Cache API are used to implement them. A cookie-based fallback is available for browsers that lack Service Worker support.

### HTTPS Certificate

While the HTTP/2 specification allows unencrypted connections, web browsers strictly enforce HTTPS.

If no certificate and key are provided, one pair will be auto-generated. The generated certificate is only valid for `localhost`. It is stored in `~/.commonshost`. As a user convenience, the certificate is added as trusted to the operating system so browsers will accept the certificate. A password prompt may appear to confirm. This is currently only supported on macOS, Windows, and Linux.

In production use [Let's Encrypt](https://letsencrypt.org) or any other trusted, signed certificate.

Intermediate certificates are stapled to the OCSP response to speed up the TLS handshake.

## See Also

- [commons.host](https://commons.host) — Static site hosting and CDN based on this server.
- [Unbundle](https://www.npmjs.com/package/unbundle) — Build tool for front-end JavaScript applications, designed for HTTP/2 Server Push and Cache Digests.

## Colophon

Made with ❤️ by Sebastiaan Deckers in 🇸🇬 Singapore.
