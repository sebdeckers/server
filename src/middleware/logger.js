const pino = require('pino')
const {getHost} = require('../helpers/getHost')

module.exports.logger = (options) => {
  const log = pino(options)
  return async function logger (request, response, next) {
    request.log = response.log = log
    log.info(`HTTP/${request.httpVersion} ${request.method} https://${getHost(request)}${request.url}`)
    next()
  }
}
