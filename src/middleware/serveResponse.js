const Negotiator = require('negotiator')
const {createReadStream, stat} = require('fs')
const {promisify} = require('util')
const {buildHeaders} = require('../helpers/buildHeaders')
const {isLoopback} = require('../helpers/isLoopback')
const {sync: readPkgUp} = require('read-pkg-up')
const osName = require('os-name')
const {serveDependencies} = require('./serveDependencies')
const {NotFound, InternalServerError} = require('http-errors')

function serverHeader () {
  const {pkg} = readPkgUp({cwd: __dirname})
  const {name, version} = pkg
  const {node, nghttp2} = process.versions
  const products = [
    `https://commons.host ${name} ${version}`,
    `node ${node}`,
    `nghttp2 ${nghttp2}`,
    `${osName()}`
  ]
  return products.join(', ')
}
const HEADER_SERVER = serverHeader()

module.exports.serveResponse = ({signature}) => {
  return async function serveResponse (request, response, next) {
    const {resolved, headers} = buildHeaders(
      request.resolved,
      request.options.cacheControl.immutable,
      new Negotiator(request).encodings(),
      request.fileIndex
    )

    if (signature === true) {
      headers.server = HEADER_SERVER
    }

    if (!isLoopback(request)) {
      headers['strict-transport-security'] =
        `max-age=${request.options.strictTransportSecurity.maxAge}`
    }

    const statusCode = response.statusCode || 200

    if (request.httpVersionMajor === 2) {
      // HTTP/2 GET
      request.once('error', next)
      response.once('error', next)
      response.stream.once('error', next)
      if (statusCode !== 200) {
        headers[':status'] = statusCode
      }
      response.stream.respondWithFile(resolved.absolute, headers, {
        statCheck (stat, headers) {
          if (request.method === 'HEAD') {
            // HTTP/2 HEAD
            headers['content-length'] = stat.size
            response.stream.respond(headers)
            return false
          }
        },
        onError ({code}) {
          if (code === 'ENOENT') {
            next(new NotFound())
          } else {
            next(new InternalServerError())
          }
        }
      })
      try {
        await serveDependencies(request, response)
      } catch (error) {
        request.log.error(error)
      }
    } else {
      try {
        response.setHeader(
          'content-length',
          (await promisify(stat)(resolved.absolute)).size
        )
      } catch (error) {
        return next(error)
      }
      if (request.method === 'HEAD') {
        // HTTP/1 HEAD
        response.writeHead(statusCode, headers)
        response.flushHeaders()
        response.end()
      } else {
        // HTTP/1 GET
        const file = createReadStream(resolved.absolute)
        file.once('open', async () => {
          response.writeHead(statusCode, headers)
          file.pipe(response)
        })
        file.once('error', (error) => {
          file.removeAllListeners()
          if (!response.headersSent) next(error)
          else if (!response.finished) response.end()
        })
        file.once('end', () => {
          file.removeAllListeners()
        })
      }
    }
  }
}
