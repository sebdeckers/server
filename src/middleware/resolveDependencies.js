const Cookies = require('cookies')
const {getDependencies} = require('../helpers/getDependencies')
const {cacheDigestFilter} = require('../helpers/cacheDigestFilter')
const {checkImmutable} = require('../helpers/checkImmutable')
const {getOrigin} = require('../helpers/getOrigin')
const {requestDestination} = require('request-destination')
const {computeDigestValue} = require('cache-digest')

const HTTP2_PRIORITY_DEFAULT = 16

async function resolveDependencies (request, response, next) {
  if ((request.httpVersionMajor === 2 && !response.stream.pushAllowed) ||
    request.options.manifest.length === 0
  ) {
    return next()
  }

  const baseUrl = getOrigin(request)
  const cookies = new Cookies(request, response)
  const pushedImmutables = []
  request.pushResponses = []

  const dependencies = getDependencies(
    request.options.manifest,
    request.fileIndex.relative,
    request.resolved.relative
  )

  const cacheDigestContains = cacheDigestFilter(request, cookies, baseUrl)

  if (request.httpVersionMajor === 2 && response.stream.pushAllowed) {
    const headers = {
      ':method': request.method,
      ':scheme': 'https',
      ':authority': request.authority
    }
    const pushStream = (headers, weight) => new Promise((resolve, reject) => {
      const {stream} = response
      stream.pushStream(headers, (error, stream) => {
        if (error) return reject(error)
        else resolve(stream)
      })
      if (weight !== HTTP2_PRIORITY_DEFAULT) {
        stream.priority({weight, silent: true})
      }
    })
    const pushPromises = []
    for (const [relative, priority] of dependencies) {
      const dependency = request.fileIndex.relative.get(relative)
      if (!cacheDigestContains(dependency.pathname)) continue
      headers[':path'] = dependency.pathname
      response.log.info(`PUSH ${baseUrl}${dependency.pathname}`)
      const pushPromise = pushStream(headers, priority)
        .then((pushResponse) => ({pushResponse, dependency}))
      pushPromises.push(pushPromise)
      if (checkImmutable(
        relative,
        request.options.cacheControl.immutable
      )) {
        dependency.isImmutable = true
        if (request.method === 'GET') {
          pushedImmutables.push(dependency.pathname)
        }
      }
    }
    request.pushResponses = await Promise.all(pushPromises)
  } else if (request.httpVersionMajor === 1) {
    const links = []
    for (const [relative] of dependencies) {
      const {pathname} = request.fileIndex.relative.get(relative)
      if (!cacheDigestContains(pathname)) continue
      const destination = requestDestination(relative)
      links.push(`<${pathname}>; rel=preload; as=${destination}`)
    }
    response.setHeader('link', links)
  }

  if (request.method === 'GET') {
    if (request.headers['cache-digest']) {
      if (cookies.get('cache-digest')) {
        cookies.set('cache-digest')
      }
    }
  }

  if (pushedImmutables.length) {
    const urls = pushedImmutables.map((pathname) => [baseUrl + pathname, null])
    const digestValue = computeDigestValue(false, urls, 2 ** 7)
    const cookie = Buffer.from(digestValue).toString('base64').replace(/=+$/, '')
    cookies.set('cache-digest', cookie)
  }

  next()
}

module.exports.resolveDependencies = () => resolveDependencies
