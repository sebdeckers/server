const {serveResponse} = require('./serveResponse')
const pino = require('pino')
const accepts = require('accepts')
const parseUrl = require('parseurl')
const {extname} = require('path')
const {createReadStream} = require('fs')

module.exports.serveFallback = (options) => {
  const log = pino(options)
  const passthrough = serveResponse(options)
  return function serveFallback (error, request, response, next) {
    if (request.options === undefined) {
      if (parseInt(error.statusCode) === 404 && options.placeholder.path) {
        const accepted = accepts(request)
        const url = parseUrl(request)
        const extension = extname(url.pathname)
        if (accepted.type('text/html') &&
          (extension === '' || extension === '.html')
        ) {
          response.setHeader('content-type', 'text/html; charset=utf-8')
          const file = createReadStream(options.placeholder.path)
          file.once('error', (error) => {
            log.error(error)
            next(error)
          })
          file.pipe(response)
          return
        }
      }
    } else {
      const fallback = request.options.fallback[error.statusCode]
      if (fallback !== undefined) {
        const {fileIndex: {absolute}} = request
        if (absolute.has(fallback)) {
          response.statusCode = parseInt(error.statusCode)
          request.resolved = absolute.get(fallback)
          passthrough(request, response, next)
          return
        }
      }
    }
    next(error)
  }
}
