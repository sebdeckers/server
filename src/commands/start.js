const {Master} = require('../master')
const {lock, unlock} = require('../helpers/pid')
const chalk = require('chalk')

module.exports.command = ['start']
module.exports.aliases = ['run', 'up', 'launch']
module.exports.desc = 'Start the server'
module.exports.builder = {
  options: {
    type: 'string',
    describe: 'Configuration file path'
  },
  watch: {
    type: 'boolean',
    describe: 'Monitor file changes',
    default: false
  }
}

module.exports.handler = async ({options, watch}) => {
  const master = new Master({
    options,
    watch,
    generateCertificate: true
  })

  process.on('exit', () => {
    unlock()
  })

  process.on('SIGINT', async () => {
    await master.close()
    console.log('')
    process.exit()
  })

  process.on('SIGHUP', () => {
    master.log.info('Server reloading')
    try {
      master.reload()
    } catch (error) {
      master.log.error(error)
    }
  })

  lock()
  try {
    await master.listen()
  } catch (error) {
    console.error()
    console.error(chalk.bold.red(error.toString()))
    // console.error(error.stack.replace(error.toString(), ''))
    console.error()
    process.exit(1)
  }
  master.log.info('Server started')
}
