const {promisify} = require('util')
const http = require('http')
const parseUrl = require('parseurl')
const send = require('send')
const {untilBefore} = require('until-before')
const {getHost} = require('./helpers/getHost')
const pino = require('pino')

const MOVED_PERMANENTLY = 301
const PERMANENT_REDIRECT = 308
const BAD_REQUEST = 400
const INTERNAL_SERVER_ERROR = 500
const BAD_GATEWAY = 502

const ACME_PREFIX = '/.well-known/acme-challenge/'

const DEFAULT_PORT_HTTPS = 443

module.exports.redirect = async (options) => {
  const log = pino(options.log)
  function requestListener (request, response) {
    const host = getHost(request)
    const hostname = untilBefore.call(host, ':')
    const {pathname} = parseUrl(request)

    log.info(`HTTP/${request.httpVersion} ${request.method} http://${host}${request.url}`)

    if (pathname.startsWith(ACME_PREFIX)) {
      if (options.acme.proxy.length > 0) {
        const url = options.acme.proxy + pathname
        log.info(`PROXY ${url}`)
        const passthrough = http.request(url, (upstream) => {
          upstream.pipe(response, {end: true})
        })
        passthrough.once('error', (error) => {
          log.error(error.message)
          response.writeHead(BAD_GATEWAY)
          response.end()
        })
        request.pipe(passthrough, {end: true})
        return
      } else if (options.acme.redirect.length > 0) {
        const location = options.acme.redirect + pathname
        log.info(`${MOVED_PERMANENTLY} ${location}`)
        response.writeHead(MOVED_PERMANENTLY, {location})
        response.end()
        return
      } else if (options.acme.webroot.length > 0) {
        const {pathname} = parseUrl(request)
        const sendOptions = {
          root: options.acme.webroot,
          etag: false,
          lastModified: false
        }
        log.info(`WEBROOT ${pathname}`)
        send(request, pathname, sendOptions)
          .on('error', (error) => {
            response.statusCode = error.status || INTERNAL_SERVER_ERROR
            response.end()
          })
          .pipe(response)
        return
      }
    }

    if (hostname === '') {
      response.writeHead(BAD_REQUEST)
      response.end()
    } else {
      const location = options.http.to === DEFAULT_PORT_HTTPS
        ? `https://${hostname}${request.url}`
        : `https://${hostname}:${options.http.to}${request.url}`
      log.info(`${PERMANENT_REDIRECT} ${location}`)
      response.writeHead(PERMANENT_REDIRECT, {location})
      response.end()
    }
  }
  const server = http.createServer(requestListener)
  await promisify(server.listen.bind(server))(options.http.from)
  return server
}
