require('hard-rejection/register')
const {server} = require('./server')

process.title = 'commonshost-worker'

process.once('message', ({options, files}) => {
  const app = server(options, files)
  app.listen(options.https.port)
})
