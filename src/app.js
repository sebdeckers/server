const connect = require('connect')
const {hostOptions} = require('./middleware/hostOptions')
const {logger} = require('./middleware/logger')
const {allowCors} = require('./middleware/allowCors')
const {allowedMethods} = require('./middleware/allowedMethods')
const {serviceWorkerScope} = require('./middleware/serviceWorkerScope')

const {resolveRequest} = require('./middleware/resolveRequest')
const {resolveDependencies} = require('./middleware/resolveDependencies')
const {serveResponse} = require('./middleware/serveResponse')
const {serveFallback} = require('./middleware/serveFallback')

const {errorHandler} = require('middleware-plain-error-handler')

module.exports.app = (options, files) => {
  const app = connect()

  app.use(hostOptions(options, files))
  app.use(logger(options.log))
  app.use(allowedMethods(['GET', 'HEAD', 'OPTIONS']))
  app.use(serviceWorkerScope())
  app.use(allowCors())

  app.use(resolveRequest())
  app.use(resolveDependencies())
  app.use(serveResponse(options))
  app.use(serveFallback(options))

  app.use(errorHandler())
  return app
}
