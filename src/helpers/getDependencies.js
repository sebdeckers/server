const {trace} = require('@commonshost/manifest')

const cachedArrays = new WeakMap()

module.exports.getDependencies =
function getDependencies (manifest, files, entry) {
  let filepaths
  if (cachedArrays.has(files)) {
    filepaths = cachedArrays.get(files)
  } else {
    filepaths = Array.from(files.keys())
    cachedArrays.set(files, filepaths)
  }

  const dependencies = new Map()
  trace(manifest, filepaths, entry, dependencies)
  dependencies.delete(entry)
  return dependencies
}
