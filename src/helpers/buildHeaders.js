const mime = require('mime')
const compressible = require('compressible')
const {checkImmutable} = require('./checkImmutable')

const ONE_SECOND = 1
const ONE_YEAR = 31536e3

const compressors = [
  {extension: '.br', encoding: 'br'},
  {extension: '.gz', encoding: 'gzip'},
  {extension: '.deflate', encoding: 'deflate'}
]

module.exports.buildHeaders = function buildHeaders (
  sourceFile,
  immutablePatterns,
  supportedEncodings,
  fileIndex
) {
  let resolved = sourceFile
  const headers = {}
  const type = mime.getType(sourceFile.absolute)
  headers['content-type'] = type.startsWith('text/')
    ? `${type}; charset=utf-8`
    : type

  const isImmutable = checkImmutable(sourceFile.absolute, immutablePatterns)
  headers['cache-control'] = isImmutable
    ? `public, max-age=${ONE_YEAR}, immutable`
    : `public, max-age=${ONE_SECOND}, must-revalidate`

  if (compressible(type)) {
    for (const {extension, encoding} of compressors) {
      const variant = sourceFile.absolute + extension
      if (fileIndex.absolute.has(variant) &&
        supportedEncodings.includes(encoding)
      ) {
        headers['content-encoding'] = encoding
        resolved = fileIndex.absolute.get(variant)
        break
      }
    }
  }
  return {headers, resolved}
}
