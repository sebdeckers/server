const test = require('blue-tape')
const {Master} = require('..')
const {join} = require('path')
const fetch = require('node-fetch')
const {Agent} = require('https')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures/public')
  const options = undefined
  master = new Master({cwd, options})
  await master.listen()
})

test('Default root is cwd', async (t) => {
  const url = 'https://localhost:8443/'
  const response = await fetch(url, {
    redirect: 'manual',
    agent: new Agent({
      rejectUnauthorized: false,
      lookup: (hostname, options, callback) => {
        callback(null, '127.0.0.1', 4)
      }
    })
  })

  t.is(response.status, 200)
})

test('stop server', async (t) => master.close())
