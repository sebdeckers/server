const test = require('blue-tape')
const {Master} = require('..')
const {join} = require('path')
const fetch = require('node-fetch')
const {Agent} = require('https')

const fixtures = [
  {
    given: {serveDefaultSite: false},
    expected: 404
  },
  {
    given: {serveDefaultSite: true},
    expected: 200
  }
]

for (const {given, expected} of fixtures) {
  let master
  test('start server', async (t) => {
    const cwd = join(__dirname, 'fixtures')
    const options = {}
    master = new Master(Object.assign(
      {cwd, options},
      given
    ))
    await master.listen()
  })

  test('Default site on localhost', async (t) => {
    const url = 'https://localhost:8443/'
    const response = await fetch(url, {
      redirect: 'manual',
      agent: new Agent({
        rejectUnauthorized: false,
        lookup: (hostname, options, callback) => {
          callback(null, '127.0.0.1', 4)
        }
      })
    })

    t.is(response.status, expected)
  })

  test('stop server', async (t) => master.close())
}
