const fetch = require('node-fetch')
const https = require('https')
const http = require('http')

async function receiveHttp1 (url, options) {
  const {Agent} = url.startsWith('https:') ? https : http
  const response = await fetch(url, Object.assign({
    agent: new Agent({
      rejectUnauthorized: false,
      lookup: (hostname, options, callback) => {
        callback(null, '127.0.0.1', 4)
      }
    })
  }, options))
  return response
}

module.exports.receiveHttp1 = receiveHttp1
