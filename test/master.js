const test = require('blue-tape')
const {Master} = require('..')
const {join} = require('path')

test('Create a server, listen, close', async (t) => {
  const master = new Master({
    options: {},
    cwd: join(__dirname, 'fixtures')
  })
  await master.listen()
  await master.close()
})
