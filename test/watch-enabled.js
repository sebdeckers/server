const test = require('blue-tape')
const {Master} = require('..')
const {join, basename} = require('path')
const {h2: receive} = require('./helpers/receive')
const tmp = require('tmp-promise')

// tmp.setGracefulCleanup()

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    hosts: [{
      domain: 'localhost',
      manifest: [{get: '**/*.html', push: '**/*.js'}]
    }]
  }
  master = new Master({cwd, options, watch: true})
  await master.listen()
})

const url = 'https://localhost:8443'
const options = {rejectUnauthorized: false}

test('Normal response and push promise', async (t) => {
  const {result, push} = await receive(url, options)
  t.is(result.response[':status'], 200)
  t.is(push.length, 1)
  t.ok(push.find(({request}) => {
    return request[':path'] === encodeURI('/foo.💩.js')
  }))
})

let file

test('Add a new file', async (t) => {
  file = await tmp.file({
    dir: join(__dirname, 'fixtures/public'),
    prefix: 'temp-',
    postfix: '.js'
  })
})

test('Wait a bit', (t) => setTimeout(t.end, 4000))

test('Updated push promises', async (t) => {
  const {result, push} = await receive(url, options)
  t.is(result.response[':status'], 200)
  t.is(push.length, 2)
  t.ok(push.find(({request}) => {
    return request[':path'] === encodeURI('/foo.💩.js')
  }))
  t.ok(push.find(({request}) => {
    return request[':path'] === encodeURI(`/${basename(file.path)}`)
  }))
})

test('cleanup temp file', async (t) => file.cleanup())
test('stop server', async (t) => master.close())
