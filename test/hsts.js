const test = require('blue-tape')
const {Master} = require('..')
const {join} = require('path')
const {connect} = require('http2')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    hosts: [
      {domain: 'localhost'},
      {domain: 'default.example.net'},
      {
        domain: 'custom.example.net',
        strictTransportSecurity: {maxAge: 1234567890}
      }
    ]
  }
  master = new Master({cwd, options})
  await master.listen()
})

const fixtures = [
  {
    label: 'Never set HSTS header on loopback hosts',
    host: 'localhost',
    hsts: undefined
  },
  {
    label: 'Sensible default HSTS header on public hosts',
    host: 'default.example.net',
    hsts: `max-age=${60 * 24 * 3600}`
  },
  {
    label: 'Configurable HSTS header on public hosts',
    host: 'custom.example.net',
    hsts: 'max-age=1234567890'
  }
]

for (const {label, host, hsts} of fixtures) {
  test(label, (t) => {
    const session = connect(
      `https://${host}:8443`,
      {
        rejectUnauthorized: false,
        lookup: (hostname, options, callback) => {
          callback(null, '127.0.0.1', 4)
        }
      }
    )

    const stream = session.request({':path': '/'})
    stream.on('socketError', t.end)
    stream.on('error', t.end)

    stream.on('response', (headers) => {
      t.is(headers['strict-transport-security'], hsts)
      stream.resume()
      stream.on('end', () => stream.close())
      stream.on('close', () => session.close(t.end))
    })
  })
}

test('stop server', async (t) => master.close())
