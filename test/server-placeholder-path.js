const test = require('blue-tape')
const {Master} = require('..')
const {join} = require('path')
const fetch = require('node-fetch')
const {Agent} = require('https')

const fixtures = [
  {
    scenario: 'Respond with placeholder',
    given: {
      options: {
        placeholder: {
          path: join(__dirname, 'fixtures/placeholder.html')
        }
      },
      pathname: '/'
    },
    expected: {
      status: 200,
      contentType: 'text/html; charset=utf-8',
      body: 'Unknown domain placeholder site.'
    }
  },
  {
    scenario: 'Request does not allow HTML',
    given: {
      options: {
        placeholder: {
          path: join(__dirname, 'fixtures/placeholder.html')
        }
      },
      pathname: '/favicon.ico'
    },
    expected: {
      status: 404,
      contentType: 'text/plain; charset=utf-8',
      body: 'Not Found'
    }
  },
  {
    scenario: 'Incorrect placeholder',
    given: {
      options: {
        placeholder: {
          path: join(__dirname, 'fixtures/does-not-exist')
        }
      },
      pathname: '/'
    },
    expected: {
      status: 500,
      contentType: 'text/plain; charset=utf-8',
      body: 'Internal Server Error'
    }
  },
  {
    scenario: 'Default behaviour',
    given: {
      options: {
        placeholder: {
        }
      },
      pathname: '/'
    },
    expected: {
      status: 404,
      contentType: 'text/plain; charset=utf-8',
      body: 'Not Found'
    }
  }
]

for (const {scenario, given, expected} of fixtures) {
  let master
  test('start server', async (t) => {
    const cwd = join(__dirname, 'fixtures')
    master = new Master({cwd, options: given.options})
    await master.listen()
  })

  test(`Request missing domain: ${scenario}`, async (t) => {
    const url = `https://foobar.localhost:8443${given.pathname}`
    const response = await fetch(url, {
      redirect: 'manual',
      agent: new Agent({
        rejectUnauthorized: false,
        lookup: (hostname, options, callback) => {
          callback(null, '127.0.0.1', 4)
        }
      })
    })
    t.is(response.status, expected.status)
    t.is(response.headers.get('content-type'), expected.contentType)
    t.is(await response.text(), expected.body)
  })

  test('stop server', async (t) => master.close())
}
