const test = require('blue-tape')
const {Master} = require('..')
const {join} = require('path')
const {h2: receive} = require('./helpers/receive')

const url = 'https://example.net:8443/'

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {}
  master = new Master({cwd, options})
  await master.listen()
})

test('Site not deployed', async (t) => {
  const {result, push} = await receive(url)
  const NOT_FOUND = 404
  t.is(result.response[':status'], NOT_FOUND)
  t.is(push.length, 0)
})

test('Deploy website with inline manifest', async (t) => {
  const root = join(__dirname, 'fixtures/public')
  const configuration = {
    domain: 'example.net',
    root,
    manifest: [
      {
        get: '/index.html',
        push: '/*.txt'
      }
    ]
  }
  await master.message({
    type: 'site-deploy',
    domain: 'example.net',
    root,
    hasNewFiles: true,
    hasNewConfiguration: true,
    configuration
  })
})

test('Inline manifest is allowed', async (t) => {
  const {result, push} = await receive(url)
  const OK = 200
  t.is(result.response[':status'], OK)
  t.is(push.length, 1)
  t.is(push[0].request[':path'], '/stuff.txt')
})

test('Deploy website with external manifest', async (t) => {
  const root = join(__dirname, 'fixtures/public')
  const configuration = {
    domain: 'example.net',
    root,
    manifest: 'manifest.json'
  }
  await master.message({
    type: 'site-deploy',
    domain: 'example.net',
    root,
    hasNewFiles: true,
    hasNewConfiguration: true,
    configuration
  })
})

test('External manifest is ignored', async (t) => {
  const {result, push} = await receive(url)
  const OK = 200
  t.is(result.response[':status'], OK)
  t.is(push.length, 0)
})

test('stop server', async (t) => master.close())

test('Ignore external manifest by default', async (t) => {
  const master = new Master({
    options: {
      hosts: [
        {
          manifest: 'manifest.json'
        }
      ]
    },
    cwd: join(__dirname, 'fixtures')
  })
  await master.listen()
  const url = 'https://localhost:8443/'
  const {push} = await receive(url)
  t.is(push.length, 0)
  await master.close()
})

test('Allow external manifest with configuration filepath', async (t) => {
  const master = new Master({
    options: {
      hosts: [
        {
          manifest: 'manifest.json'
        }
      ]
    },
    configurationFilepath: join(__dirname, 'fixtures/external-manifest/fake'),
    cwd: join(__dirname, 'fixtures')
  })
  await master.listen()
  const url = 'https://localhost:8443/'
  const {push} = await receive(url)
  t.is(push.length, 2)
  await master.close()
})
