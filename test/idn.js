const test = require('blue-tape')
const {Master} = require('..')
const {join} = require('path')
const {connect} = require('http2')
const {domainToASCII: toASCII} = require('url')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {hosts: [{domain: toASCII('💩.localhost')}]}
  master = new Master({cwd, options})
  await master.listen()
})

test('Punycode IDN hostname', (t) => {
  const session = connect(
    `https://${toASCII('💩.localhost')}:8443`,
    {
      rejectUnauthorized: false,
      lookup: (hostname, options, callback) => {
        callback(null, '127.0.0.1', 4)
      }
    }
  )

  const stream = session.request({':path': '/'})

  stream.on('response', (headers) => {
    t.is(headers[':status'], 200)
    stream.resume()
    stream.on('end', () => {
      session.close(t.end)
    })
  })

  session.on('socketError', t.end)
  session.on('error', t.end)
  stream.on('error', t.end)
})

test('stop server', async (t) => master.close())
