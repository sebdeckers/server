const test = require('blue-tape')
const {Master} = require('..')
const {join} = require('path')

test('Must have a worker', async (t) => {
  const master = new Master({
    options: {workers: {count: 0}},
    cwd: join(__dirname, 'fixtures')
  })
  await t.shouldFail(master.listen())
})
