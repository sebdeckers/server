const test = require('blue-tape')
const {Master} = require('..')
const {join} = require('path')
const {connect} = require('http2')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {}
  master = new Master({cwd, options})
  await master.listen()
})

const HTTP_STATUS = {
  OK: 200,
  METHOD_NOT_ALLOWED: 405
}

const fixtures = [
  {method: 'GET', status: HTTP_STATUS.OK},
  {method: 'HEAD', status: HTTP_STATUS.OK},
  {method: 'OPTIONS', status: HTTP_STATUS.OK},
  {method: 'POST', status: HTTP_STATUS.METHOD_NOT_ALLOWED},
  {method: 'PUT', status: HTTP_STATUS.METHOD_NOT_ALLOWED},
  {method: 'PATCH', status: HTTP_STATUS.METHOD_NOT_ALLOWED},
  {method: 'FOOBAR', status: HTTP_STATUS.METHOD_NOT_ALLOWED}
]

let session
test('Connect to server', async (t) => {
  session = connect(
    'https://localhost:8443',
    {rejectUnauthorized: false}
  )
  session.on('socketError', t.fail)
  session.on('error', t.fail)
})

for (const {method, status} of fixtures) {
  test(`${method} method returns status code ${status}`, (t) => {
    const stream = session.request({':method': method, ':path': '/'})

    stream.on('response', (headers) => {
      t.is(headers[':status'], status)
      stream.on('close', () => t.end())
      stream.on('end', () => stream.close())
      stream.resume()
    })

    stream.on('error', t.end)
  })
}

test('Disconnect from server', (t) => session.close(t.end))
test('stop server', async (t) => master.close())
