const test = require('blue-tape')
const {Master} = require('..')
const {join} = require('path')
const fetch = require('node-fetch')
const {Agent} = require('http')
const {domainToASCII: toASCII} = require('url')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {}
  master = new Master({cwd, options})
  await master.listen()
})

test('HTTP to HTTPS redirect', async (t) => {
  const url = `http://${toASCII('💩.localhost')}:8080/foo`
  const response = await fetch(url, {
    redirect: 'manual',
    agent: new Agent({
      lookup: (hostname, options, callback) => {
        callback(null, '127.0.0.1', 4)
      }
    })
  })

  t.is(response.status, 308)
  t.is(response.statusText, 'Permanent Redirect')
  t.is(
    response.headers.get('location'),
    `https://${toASCII('💩.localhost')}:8443/foo`
  )
})

test('stop server', async (t) => master.close())
